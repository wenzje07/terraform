terraform {
  backend "consul" {
    path    = "terraform/state/google-joel"
	lock = true
  }
}
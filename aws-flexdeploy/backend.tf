terraform {
  backend "consul" {
    path    = "terraform/state/flex-aws/${{FD_ENVIRONMENT_CODE}}"
	lock = true
  }
}